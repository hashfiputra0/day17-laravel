@extends('master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header-->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Cast {{$cast->id}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="/cast">Cast</a></li>
                        <li class="breadcrumb-item active">Edit Cast {{$cast->id}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content header-->

    <!-- Main content -->
    <section class="content">

        <!-- Card -->
        <div class="container px-5 py-1">
            <div class="card mx-5">

                <!-- Card Body -->
                <div class="card-body">
                    <div>
                        <form action="/cast/{{$cast->id}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama"
                                    placeholder="Masukkan Nama">
                                @error('nama')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="umur">Umur</label>
                                <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur"
                                    placeholder="Masukkan Umur">
                                @error('umur')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="bio">Bio</label>
                                <textarea class="form-control" name="bio" id="bio"
                                    placeholder="Masukkan Bio" rows="5">{{$cast->bio}}</textarea>
                                @error('bio')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

        </div>
        <!-- /.card -->

    </section>
    <!-- /.main content -->

</div>
<!-- /.content-wrapper -->
@endsection