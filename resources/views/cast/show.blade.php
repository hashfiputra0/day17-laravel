@extends('master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  
  <!-- Content Header -->
  <section class="content-header">

    <!-- Container Fluid -->
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Show Cast {{$cast->id}}</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/cast">Cast</a></li>
            <li class="breadcrumb-item active">Show Cast {{$cast->id}}</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->

  </section>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content px-5 py-1">

    <!-- Card -->
    <div class="card ">

      <!-- Card Body -->
      <div class="card-body">
        <h1>{{$cast->nama}} ({{$cast->umur}})</h1>
        <h4>Biodata</h4>
        <p>{{$cast->bio}}</p>
      </div>
      <!-- /.card-body -->

    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection