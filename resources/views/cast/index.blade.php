@extends('master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header -->
    <div class="content-header">

        <!-- Container Fluid -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Cast</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Cast</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

        <!-- Container Fluid -->
        <div class="container-fluid py-1">

            <!-- Card -->
            <div class="card">

                <!-- Card Header -->
                <div class="card-header">
                    <a href="/cast/create" class="btn btn-primary w-100">Tambah Cast</a>
                </div>
                <!-- /.card-header -->

                <!-- Card Body -->
                <div class="card-body pt-0">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Umur</th>
                                <th scope="col">Biodata</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($cast as $key=>$value)
                            <tr>
                                <td>{{$key + 1}}</th>
                                <td>{{$value->nama}}</td>
                                <td>{{$value->umur}}</td>
                                <td>{{$value->bio}}</td>
                                <td>
                                    <a href="/cast/{{$value->id}}" class="btn btn-info w-100">Show</a>
                                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary my-1 w-100">Edit</a>
                                    <form action="/cast/{{$value->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger w-100" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr colspan="3">
                                <td>No data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->

            </div>
            <!-- /.card -->

        </div>
        <!-- /.container-fluid -->

    </section>
    <!-- /.main content -->

</div>
<!-- /.content-wrapper -->
@endsection

@push('js-cast')
<!-- Cast Script -->
<script src="../plugins/datatables/jquery.dataTables.js"></script>
<script src="../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function () {
        $("#example1").DataTable();
      });
</script>
@endpush

@push('css-cast')
<!-- Cast -->
<link rel="stylesheet" href="../plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush