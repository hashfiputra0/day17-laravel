<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    // Menampilkan list data para pemain film
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    // Menampilkan detail data pemain film dengan id tertentu
    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    // Menampilkan form untuk membuat data pemain film baru
    public function create()
    {
        return view('cast.create');
    }

    // Menyimpan data baru ke tabel Cast
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        Cast::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);
        return redirect('/cast');
    }

    // 	Menampilkan form untuk edit pemain film dengan id tertentu
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    // 	Menyimpan perubahan data pemain film (update) untuk id tertentu
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->update();

        return redirect('/cast');
    }

    // 	Menghapus data pemain film dengan id tertentu
    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
