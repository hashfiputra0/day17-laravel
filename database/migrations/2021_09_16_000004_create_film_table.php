<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'film';

    /**
     * Run the migrations.
     * @table film
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('judul', 45);
            $table->text('ringkasan');
            $table->integer('tahun');
            $table->string('poster', 45);
            $table->integer('genre_id')->unsigned();
            $table->timestamp('created_at');

            $table->index(["genre_id"], 'fk_film_genre1_idx');


            $table->foreign('genre_id', 'fk_film_genre1_idx')
                ->references('id')->on('genre')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
