<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKritikTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'kritik';

    /**
     * Run the migrations.
     * @table kritik
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('film_id')->unsigned();
            $table->text('isi');
            $table->integer('point');
            $table->timestamp('created_at');

            $table->index(["user_id"], 'fk_kritik_user1_idx');

            $table->index(["film_id"], 'fk_kritik_film1_idx');


            $table->foreign('user_id', 'fk_kritik_user1_idx')
                ->references('id')->on('user')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('film_id', 'fk_kritik_film1_idx')
                ->references('id')->on('film')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
