<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeranTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'peran';

    /**
     * Run the migrations.
     * @table peran
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('film_id')->unsigned();
            $table->integer('cast_id')->unsigned();
            $table->string('nama', 45);

            $table->index(["film_id"], 'fk_peran_film1_idx');

            $table->index(["cast_id"], 'fk_peran_cast1_idx');


            $table->foreign('film_id', 'fk_peran_film1_idx')
                ->references('id')->on('film')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cast_id', 'fk_peran_cast1_idx')
                ->references('id')->on('cast')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
